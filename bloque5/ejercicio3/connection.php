<?php
$server = "127.0.0.1:6033";
$db = "app_db";
$user = "db_user";
$pass = "db_pass";

try {
  $conn = new PDO("mysql:host=$server;dbname=$db", $user, $pass);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Connected successfully\n";
} catch(PDOException $e) {
  die("Connection failed: " . $e->getMessage());
}

$sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA;";

foreach ($conn->query($sql) as $row) {
  print $row["SCHEMA_NAME"] . "\n";
}

?>
